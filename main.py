import pygame
import random
import math

from pygame import mixer

#initialized the pygame
pygame.init() 

#create the screen
screen = pygame.display.set_mode((800, 600))

# Background
background = pygame.image.load('background.png')
# Add Title and Icon
pygame.display.set_caption("Space Invaders")
icon = pygame.image.load('ufo.png')
pygame.display.set_icon(icon)

# Background Sound
# mixer.music.load('background.wav')
# mixer.music.play(-1)

#player
playerImag = pygame.image.load('spc.png')
playerX = 370
playerY = 480
playerX_change = 0

# Enemy
enemyImag = []
# Random Location
enemyX = []
enemyY =  []
enemyX_change = []
enemyY_change = []
num_of_enemies = 20

for i in range(num_of_enemies):
	enemyImag.append(pygame.image.load('enimy.png'))
	# Random Location
	enemyX.append(random.randint(0, 735))
	enemyY.append(random.randint(50, 150))
	enemyX_change.append(2)
	enemyY_change.append(40)

# Bullet
# Ready - You can't see the bullet on the screen 
# Fire - The bullet is currently moving.
bulletImag = pygame.image.load('bullet.png')
# Random Location
bulletX = 0
bulletY = 480
bulletX_change = 0
bulletY_change = 10
bullet_state = "ready"

#score 
score_value = 0
font = pygame.font.Font('freesansbold.ttf', 32)

textX = 10
textY = 10

# Game over text
over_font = pygame.font.Font('freesansbold.ttf', 64)


def player(x,y) :
	# blit = draw playerImag in position (playerX, playerY)
	screen.blit(playerImag, (x, y))

def enemy(x,y,i):
	screen.blit(enemyImag[i], (x, y))

def fire_bullet(x,y):
	global bullet_state
	bullet_state = "fire"
	screen.blit(bulletImag, (x+10, y))

def isCollision(enemyX, enemyY, bulletX, bulletY):
	distance = math.sqrt((math.pow(enemyX - enemyY, 2)) + (math.pow(bulletX - bulletY, 2)))
	if distance < 27 :
		return True
	else:
		return False

def show_score(x, y):
	score = font.render("Score :" + str(score_value), True, (255,255,255))
	screen.blit(score, (x, y))

def game_over_text():
	over_text = font.render("GAME OVER", True, (255,255,255))
	screen.blit(over_text, (270,250))



# game loop
running = True

while running:

	# Change screen color
	screen.fill((0, 0, 0)) 
	# Background image
	screen.blit(background,(0, 0))

	#getting all events in pygame
	for event in pygame.event.get():
		#checking QUIT event is running 
		if event.type == pygame.QUIT:  
			# close the window when running is false
			running = False 

		# If keystroke is pressed check whether it's right or left
		# check whether arrow key is pressed 
		# KEYDOWN = pressed key
		if event.type == pygame.KEYDOWN:
			if event.key == pygame.K_LEFT:
				playerX_change = -5
			if event.key == pygame.K_RIGHT:
				playerX_change = 5
			if event.key == pygame.K_SPACE:
				if bullet_state=="ready":
					# bullet_sound = mixer.Sound('laser.wav')
					# bullet_sound.play()
					# get the current position of the spaceship
					bulletX = playerX
					fire_bullet(bulletX, bulletY)


		# Removal of pressed key
		if event.type == pygame.KEYUP:
			if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
				playerX_change = 0
				
	# change x value of the spaceship		
	playerX += playerX_change

	# Checking for boundaries of spaceship so it does not go out of bound
	if playerX <=0:
		playerX = 0
	elif playerX >=768 :
		playerX = 768

	# Enemy Movement
	for i in range(num_of_enemies):

		# Game over
		if enemyY[i] > 400:
			for j in range(num_of_enemies):
				enemyY[j] = 2000
			game_over_text()
			break	

		enemyX[i] += enemyX_change[i]

		if enemyX[i] <=0:
			enemyX_change[i] = 1
			enemyY[i] += enemyY_change[i]
		elif enemyX[i] >=768 :
			enemyX_change[i]= -1
			enemyY[i] += enemyY_change[i]

		#collision
		collision = isCollision(enemyX[i], enemyY[i], bulletX, bulletY)
		if collision:
			# explosion_sound = mixer.Sound('explosion.wav')
			# explosion_sound.play()
			bulletY = 480
			bullet_state = "ready"
			score_value += 1
			enemyX[i] = random.randint(0, 735)
			enemyY[i] = random.randint(50, 150)
		enemy(enemyX[i], enemyY[i], i)

	# Bullet Movement
	if bulletY <= 0:
		bulletY = 480
		bullet_state = "ready"

	if bullet_state=="fire":
		fire_bullet(bulletX, bulletY)
		bulletY -= bulletY_change

	

	#function
	player(playerX, playerY)
	show_score(textX, textY)
	pygame.display.update()


